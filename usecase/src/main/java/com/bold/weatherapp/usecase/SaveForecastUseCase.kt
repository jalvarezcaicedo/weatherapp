package com.bold.weatherapp.usecase

import com.bold.weatherapp.data.repository.ForecastRepository
import com.bold.weatherapp.domain.ForecastDomain

class SaveForecastUseCase(
    private val forecastRepository: ForecastRepository
) {
    suspend operator fun invoke(forecastDomain: ForecastDomain) =
        forecastRepository.saveForecastData(forecastDomain)

}