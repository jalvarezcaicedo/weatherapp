package com.bold.weatherapp.usecase

import arrow.core.Either
import com.bold.weatherapp.data.repository.SearchRepository
import com.bold.weatherapp.domain.Error
import com.bold.weatherapp.domain.LocationDomain

class GetRemoteSearchUseCase(
    private val searchRepository: SearchRepository
) {
    suspend operator fun invoke(query: String): Either<Error, List<LocationDomain>> =
        searchRepository.getSearchData(query)
}