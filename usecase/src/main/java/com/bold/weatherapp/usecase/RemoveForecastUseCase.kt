package com.bold.weatherapp.usecase

import com.bold.weatherapp.data.repository.ForecastRepository

class RemoveForecastUseCase(
    private val forecastRepository: ForecastRepository
) {
    suspend operator fun invoke() = forecastRepository.deleteForecastData()

}