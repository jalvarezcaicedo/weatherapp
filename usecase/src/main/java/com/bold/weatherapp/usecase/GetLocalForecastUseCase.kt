package com.bold.weatherapp.usecase

import com.bold.weatherapp.data.repository.ForecastRepository
import com.bold.weatherapp.domain.ForecastDomain
import kotlinx.coroutines.flow.Flow

class GetLocalForecastUseCase(
    private val forecastRepository: ForecastRepository
) {

    operator fun invoke(): Flow<ForecastDomain> = forecastRepository.getLocalForecastData()

}