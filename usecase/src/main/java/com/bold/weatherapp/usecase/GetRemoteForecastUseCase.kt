package com.bold.weatherapp.usecase

import arrow.core.Either
import com.bold.weatherapp.data.repository.ForecastRepository
import com.bold.weatherapp.domain.Error
import com.bold.weatherapp.domain.ForecastDomain

class GetRemoteForecastUseCase(
    private val forecastRepository: ForecastRepository
) {
    suspend operator fun invoke(query: String): Either<Error, ForecastDomain> =
        forecastRepository.getRemoteForecastData(query)
}