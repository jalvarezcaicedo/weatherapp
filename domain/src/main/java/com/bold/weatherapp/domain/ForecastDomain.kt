package com.bold.weatherapp.domain

data class ForecastDomain(
    var location: LocationDomain = LocationDomain(),
    var current: CurrentDomain = CurrentDomain(),
    var forecast: ForecastDataDomain = ForecastDataDomain()
)

data class Condition(
    var text: String? = null, var icon: String? = null
)

data class CurrentDomain(
    var tempC: Double? = null,
    var condition: Condition? = Condition(),
    var windKph: Double? = null,
    var humidity: Int? = null,
    var visKm: Int? = null
)

data class Day(
    var condition: Condition? = Condition(),
    var maxtempC: Double? = null,
    var mintempC: Double? = null,
)

data class Forecastday(
    var date: String? = null, var day: Day? = Day()
)

data class ForecastDataDomain(
    var forecastday: List<Forecastday> = arrayListOf()
)