package com.bold.weatherapp.domain

data class LocationDomain(
    var name: String? = null, var region: String? = null, var country: String? = null
)