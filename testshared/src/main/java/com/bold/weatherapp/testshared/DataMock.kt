package com.bold.weatherapp.testshared

import com.bold.weatherapp.domain.Condition
import com.bold.weatherapp.domain.CurrentDomain
import com.bold.weatherapp.domain.Day
import com.bold.weatherapp.domain.ForecastDataDomain
import com.bold.weatherapp.domain.ForecastDomain
import com.bold.weatherapp.domain.Forecastday
import com.bold.weatherapp.domain.LocationDomain


val mockLocationDomain = LocationDomain("Bogota", "Cundinamarca", "Colombia")

val mockForecastDomain = ForecastDomain(
        LocationDomain("Bogota", "Cundinamarca", "Colombia"),
        CurrentDomain(
            tempC = 17.0,
            condition = Condition("Overcast", "//cdn.weatherapi.com/weather/64x64/day/122.png"),
            windKph = 25.9,
            humidity = 63,
            visKm = 10
        ),
        ForecastDataDomain(
            listOf(
                Forecastday(
                    date = "2023-07-29",
                    day = Day(
                        Condition("Moderate rain", "//cdn.weatherapi.com/weather/64x64/day/302.png")
                    )
                ),
                Forecastday(
                    date = "2023-07-30",
                    day = Day(
                        Condition("Moderate rain", "//cdn.weatherapi.com/weather/64x64/day/302.png")
                    )
                ),
                Forecastday(
                    date = "2023-07-31",
                    day = Day(
                        Condition(
                            "Patchy rain possible",
                            "//cdn.weatherapi.com/weather/64x64/day/176.png"
                        )
                    )
                )
            )
        )
    )