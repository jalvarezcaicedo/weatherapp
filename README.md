# My recipes

A simple project to show abilities in bold challenge taken data
from [WeatherApi](https://api.weatherapi.com/) based on Kotlin MVVM with clean architecture as a framework

## Architecture

Based on mvvm architecture with event communication through UI states, use cases and repository pattern based on
clean architecture.<br><br>
![architecture](https://user-images.githubusercontent.com/24237865/44525736-e9e7b700-a71c-11e8-8045-42c4478dd67e.png)

## Design patterns

- Observer pattern: Design pattern used to communicate actions between layers and stay updated the user interface elements with emissions of business layers (servers and database layers)
- State pattern: Used to communicate the use cases responses to user interface when this objects change their state
- Builder pattern: Used inside of recycler view adapters to build each element of recycler view to control changes based of data information received in recycler data
- Singleton pattern: Used to stay a class unique instance reference

## Components

- MVVM Architecture
- Architecture Components (Lifecycle, ViewModel, Room Persistence, Flow, Navigation, Coroutines)
- ViewBinding
- [Hilt](https://dagger.dev/hilt/) for dependency injection
- [Retrofit2 & Gson](https://github.com/square/retrofit) for constructing the REST API
- [Room](https://developer.android.com/jetpack/androidx/releases/room) for abstraction layer in data persistence
- [OkHttp3](https://github.com/square/okhttp) for implementing interceptor, logging and mocking web server
- [Glide](https://github.com/bumptech/glide) for loading images
- [Mockito-kotlin](https://github.com/mockito/mockito-kotlin) for Junit mock test