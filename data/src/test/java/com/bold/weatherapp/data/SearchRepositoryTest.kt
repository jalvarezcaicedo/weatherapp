package com.bold.weatherapp.data

import arrow.core.Either
import arrow.core.right
import com.bold.weatherapp.data.repository.SearchRepository
import com.bold.weatherapp.data.source.RemoteSearchDataSource
import com.bold.weatherapp.domain.LocationDomain
import com.bold.weatherapp.testshared.mockLocationDomain
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class SearchRepositoryTest {
    @Mock
    lateinit var remoteSearchDataSource: RemoteSearchDataSource

    private lateinit var searchRepository: SearchRepository


    @Before
    fun setUp() {
        searchRepository = SearchRepository(remoteSearchDataSource)
    }

    @Test
    fun `when list of location by query success, return location list from server`() = runBlocking {
        val expectedResult: Either<Error, List<LocationDomain>> =
            Either.Right(listOf(mockLocationDomain, mockLocationDomain))
        whenever(remoteSearchDataSource.getSearchData("test"))
            .thenReturn(listOf(mockLocationDomain, mockLocationDomain).right())

        val result = searchRepository.getSearchData("test")

        assertEquals(expectedResult, result)
    }
}