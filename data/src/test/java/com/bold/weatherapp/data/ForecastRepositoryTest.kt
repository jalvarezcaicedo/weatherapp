package com.bold.weatherapp.data

import arrow.core.Either
import arrow.core.right
import com.bold.weatherapp.data.repository.ForecastRepository
import com.bold.weatherapp.data.source.LocalForecastDataSource
import com.bold.weatherapp.data.source.RemoteForecastDataSource
import com.bold.weatherapp.domain.ForecastDomain
import com.bold.weatherapp.testshared.mockForecastDomain
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class ForecastRepositoryTest {
    @Mock
    lateinit var remoteForecastDataSource: RemoteForecastDataSource

    @Mock
    lateinit var localForecastDataSource: LocalForecastDataSource

    private lateinit var forecastRepository: ForecastRepository


    @Before
    fun setUp() {
        forecastRepository = ForecastRepository(localForecastDataSource, remoteForecastDataSource)
    }

    @Test
    fun `when list of products by name success, return product list from server`() = runBlocking {
        val expectedResult: Either<Error, ForecastDomain> =
            Either.Right(mockForecastDomain)
        whenever(remoteForecastDataSource.getForecastData("test"))
            .thenReturn(mockForecastDomain.right())

        val result = forecastRepository.getRemoteForecastData("test")

        assertEquals(expectedResult, result)
    }
}