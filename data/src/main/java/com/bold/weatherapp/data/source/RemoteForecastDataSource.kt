package com.bold.weatherapp.data.source

import arrow.core.Either
import com.bold.weatherapp.domain.Error
import com.bold.weatherapp.domain.ForecastDomain

interface RemoteForecastDataSource {

    suspend fun getForecastData(query: String): Either<Error, ForecastDomain>

}