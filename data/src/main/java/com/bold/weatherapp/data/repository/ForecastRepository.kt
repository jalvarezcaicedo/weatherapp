package com.bold.weatherapp.data.repository

import arrow.core.Either
import com.bold.weatherapp.data.source.LocalForecastDataSource
import com.bold.weatherapp.data.source.RemoteForecastDataSource
import com.bold.weatherapp.domain.Error
import com.bold.weatherapp.domain.ForecastDomain
import kotlinx.coroutines.flow.Flow

class ForecastRepository(
    private val localForecastDataSource: LocalForecastDataSource,
    private val remoteForecastDataSource: RemoteForecastDataSource
) {

    suspend fun saveForecastData(forecastDomain: ForecastDomain) =
        localForecastDataSource.saveForecastData(forecastDomain)

    suspend fun deleteForecastData() = localForecastDataSource.removeForecastData()

    fun getLocalForecastData(): Flow<ForecastDomain> = localForecastDataSource.getForecastData()

    suspend fun getRemoteForecastData(query: String): Either<Error, ForecastDomain> =
        remoteForecastDataSource.getForecastData(query)

}