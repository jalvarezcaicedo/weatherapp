package com.bold.weatherapp.data.source

import com.bold.weatherapp.domain.ForecastDomain
import kotlinx.coroutines.flow.Flow

interface LocalForecastDataSource {

    fun getForecastData(): Flow<ForecastDomain>

    suspend fun saveForecastData(forecast: ForecastDomain)

    suspend fun removeForecastData()

}