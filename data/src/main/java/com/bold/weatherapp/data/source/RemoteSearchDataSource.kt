package com.bold.weatherapp.data.source

import arrow.core.Either
import com.bold.weatherapp.domain.Error
import com.bold.weatherapp.domain.LocationDomain

interface RemoteSearchDataSource {

    suspend fun getSearchData(query: String): Either<Error, List<LocationDomain>>

}