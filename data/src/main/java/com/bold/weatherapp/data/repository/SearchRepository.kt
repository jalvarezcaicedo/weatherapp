package com.bold.weatherapp.data.repository

import com.bold.weatherapp.data.source.RemoteSearchDataSource

class SearchRepository(
    private val remoteSearchDataSource: RemoteSearchDataSource
) {

    suspend fun getSearchData(query: String) = remoteSearchDataSource.getSearchData(query)

}