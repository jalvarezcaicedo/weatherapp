package com.bold.weatherapp.common

import co.chiper.vivo.pos.domain.common.utils.UniqueIdGeneratorImpl

val nextUniqueId: Long
    get() {
        val uniqueIdGeneratorImpl = UniqueIdGeneratorImpl()
        return uniqueIdGeneratorImpl.nextId()
    }