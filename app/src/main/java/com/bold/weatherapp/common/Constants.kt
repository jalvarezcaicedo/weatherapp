package com.bold.weatherapp.common

object Constants {
    //ENDPOINTS
    const val SEARCH_PATH = "/v1/search.json"
    const val FORECAST_PATH = "/v1/forecast.json"
    const val FORECAST_QUERY_DAYS = "5"
}