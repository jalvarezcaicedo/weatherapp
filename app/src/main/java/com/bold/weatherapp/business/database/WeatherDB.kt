package com.bold.weatherapp.business.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.bold.weatherapp.business.database.dao.ForecastDao
import com.bold.weatherapp.business.database.entity.ForecastEntity

@Database(
    entities = [
        ForecastEntity::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class WeatherDB : RoomDatabase() {

    abstract fun forecastDao(): ForecastDao

    companion object {
        @Synchronized
        fun getDatabase(context: Context): WeatherDB = Room.databaseBuilder(
            context.applicationContext, WeatherDB::class.java, "weather_db"
        ).build()
    }

}