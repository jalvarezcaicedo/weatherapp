package com.bold.weatherapp.business.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bold.weatherapp.domain.CurrentDomain
import com.bold.weatherapp.domain.ForecastDataDomain
import com.bold.weatherapp.domain.LocationDomain

@Entity(tableName = "Forecast")
data class ForecastEntity(
    @PrimaryKey var id: String,
    var location: LocationDomain,
    var current: CurrentDomain,
    var forecast: ForecastDataDomain
)