package com.bold.weatherapp.business.database

import androidx.room.TypeConverter
import com.bold.weatherapp.domain.CurrentDomain
import com.bold.weatherapp.domain.ForecastDataDomain
import com.bold.weatherapp.domain.Forecastday
import com.bold.weatherapp.domain.LocationDomain
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {
    private val gson = Gson()

    @TypeConverter
    fun LocationDomain.locationDomainToJson(): String = gson.toJson(this)

    @TypeConverter
    fun String.toLocationDTO(): LocationDomain =
        gson.fromJson(this, object : TypeToken<LocationDomain?>() {}.type)

    @TypeConverter
    fun CurrentDomain.currentDomainToJson(): String = gson.toJson(this)

    @TypeConverter
    fun String.toCurrentDTO(): CurrentDomain =
        gson.fromJson(this, object : TypeToken<CurrentDomain?>() {}.type)

    @TypeConverter
    fun ForecastDataDomain.forecastDataDomainToJson(): String = gson.toJson(this)

    @TypeConverter
    fun String.toForecastDataDTO(): ForecastDataDomain =
        gson.fromJson(this, object : TypeToken<ForecastDataDomain?>() {}.type)

    @TypeConverter
    fun List<Forecastday>.forecastDayToJson(): String = gson.toJson(this)

    @TypeConverter
    fun String.toForecastDayList(): List<Forecastday> =
        gson.fromJson(this, object : TypeToken<List<Forecastday>?>() {}.type)
}