package com.bold.weatherapp.business.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bold.weatherapp.business.database.entity.ForecastEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ForecastDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertForecastEntity(forecastEntity: ForecastEntity)

    @Query("SELECT * FROM Forecast")
    fun findForecastEntity(): Flow<ForecastEntity>

    @Query("DELETE FROM Forecast")
    fun deleteForecastData()

}