package com.bold.weatherapp.business.database.source

import com.bold.weatherapp.business.database.WeatherDB
import com.bold.weatherapp.business.database.toForecastDomain
import com.bold.weatherapp.business.database.toForecastEntity
import com.bold.weatherapp.data.source.LocalForecastDataSource
import com.bold.weatherapp.domain.ForecastDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LocalForecastDataSourceImpl(
    private val db: WeatherDB
) : LocalForecastDataSource {

    private val forecastDao by lazy { db.forecastDao() }

    override fun getForecastData(): Flow<ForecastDomain> =
        forecastDao.findForecastEntity().map {
            it.toForecastDomain()
        }

    override suspend fun saveForecastData(forecast: ForecastDomain) =
        forecastDao.insertForecastEntity(forecast.toForecastEntity())

    override suspend fun removeForecastData() = forecastDao.deleteForecastData()
}