package com.bold.weatherapp.business.database

import com.bold.weatherapp.business.database.entity.ForecastEntity
import com.bold.weatherapp.common.nextUniqueId
import com.bold.weatherapp.domain.ForecastDomain


fun ForecastDomain.toForecastEntity() = ForecastEntity(
    nextUniqueId.toString(), location, current, forecast
)

fun ForecastEntity.toForecastDomain() = ForecastDomain(
    location, current, forecast
)