package com.bold.weatherapp.business.server

import com.bold.weatherapp.business.server.response.ConditionResponse
import com.bold.weatherapp.business.server.response.CurrentResponse
import com.bold.weatherapp.business.server.response.DayResponse
import com.bold.weatherapp.business.server.response.Forecast
import com.bold.weatherapp.business.server.response.ForecastDayResponse
import com.bold.weatherapp.business.server.response.ForecastResponse
import com.bold.weatherapp.business.server.response.LocationResponse
import com.bold.weatherapp.domain.Condition
import com.bold.weatherapp.domain.CurrentDomain
import com.bold.weatherapp.domain.Day
import com.bold.weatherapp.domain.ForecastDataDomain
import com.bold.weatherapp.domain.ForecastDomain
import com.bold.weatherapp.domain.Forecastday
import com.bold.weatherapp.domain.LocationDomain

fun List<LocationResponse>.toLocationsDomain(): List<LocationDomain> = map { it.toLocationDomain() }

fun LocationResponse.toLocationDomain() = LocationDomain(
    name = name,
    region = region,
    country = country
)

fun ForecastResponse.toForecastDomain(): ForecastDomain = ForecastDomain(
    location = LocationDomain(
        name = this.location.name,
        region = this.location.region,
        country = this.location.country
    ),
    current = CurrentDomain(
        tempC = this.current.tempC,
        condition = Condition(this.current.condition?.text, this.current.condition?.icon),
        windKph = this.current.windKph,
        humidity = this.current.humidity,
        visKm = this.current.visKm
    ),
    forecast = ForecastDataDomain(
        this.forecast.forecastday.map { it.toForecastDayDomain() }
    )
)

fun ForecastDayResponse.toForecastDayDomain(): Forecastday = Forecastday(
    this.date, Day(this.day?.condition?.toConditionDomain(), this.day?.maxtempC, this.day?.mintempC)
)

fun ConditionResponse.toConditionDomain(): Condition = Condition(
    this.text, this.icon
)

fun ForecastDomain.toForecastResponse(): ForecastResponse = ForecastResponse(
    location = LocationResponse(
        name = this.location.name,
        region = this.location.region,
        country = this.location.country
    ),
    current = CurrentResponse(
        tempC = this.current.tempC,
        condition = ConditionResponse(this.current.condition?.text, this.current.condition?.icon),
        windKph = this.current.windKph,
        humidity = this.current.humidity,
        visKm = this.current.visKm
    ),
    forecast = Forecast(this.forecast.forecastday.map { it.toForecastDayResponse() })
)

fun Forecastday.toForecastDayResponse(): ForecastDayResponse = ForecastDayResponse(
    this.date, DayResponse(this.day?.condition?.toConditionResponse())
)

fun Condition.toConditionResponse(): ConditionResponse = ConditionResponse(this.text, this.icon)