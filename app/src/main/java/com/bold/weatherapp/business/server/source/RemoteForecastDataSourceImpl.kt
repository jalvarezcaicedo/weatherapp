package com.bold.weatherapp.business.server.source

import arrow.core.Either
import com.bold.weatherapp.BuildConfig
import com.bold.weatherapp.business.server.service.WeatherApi
import com.bold.weatherapp.business.server.toForecastDomain
import com.bold.weatherapp.business.util.tryCall
import com.bold.weatherapp.common.Constants.FORECAST_QUERY_DAYS
import com.bold.weatherapp.data.source.RemoteForecastDataSource
import com.bold.weatherapp.domain.Error
import com.bold.weatherapp.domain.ForecastDomain

class RemoteForecastDataSourceImpl(
    private val weatherApi: WeatherApi
) : RemoteForecastDataSource {
    override suspend fun getForecastData(query: String): Either<Error, ForecastDomain> =
        tryCall {
            weatherApi.getForecastData(query, BuildConfig.API_KEY, FORECAST_QUERY_DAYS)
                .toForecastDomain()
        }
}