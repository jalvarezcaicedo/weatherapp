package com.bold.weatherapp.business.server.source

import arrow.core.Either
import com.bold.weatherapp.BuildConfig
import com.bold.weatherapp.business.server.service.WeatherApi
import com.bold.weatherapp.business.server.toLocationsDomain
import com.bold.weatherapp.business.util.tryCall
import com.bold.weatherapp.data.source.RemoteSearchDataSource
import com.bold.weatherapp.domain.Error
import com.bold.weatherapp.domain.LocationDomain

class RemoteSearchDataSourceImpl(
    private val weatherApi: WeatherApi
) : RemoteSearchDataSource {
    override suspend fun getSearchData(query: String): Either<Error, List<LocationDomain>> =
        tryCall {
            weatherApi.getSearchByQuery(query, BuildConfig.API_KEY).toLocationsDomain()
        }

}