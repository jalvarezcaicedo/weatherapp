package com.bold.weatherapp.business.server.service

import com.bold.weatherapp.business.server.response.ForecastResponse
import com.bold.weatherapp.business.server.response.LocationResponse
import com.bold.weatherapp.common.Constants.FORECAST_PATH
import com.bold.weatherapp.common.Constants.SEARCH_PATH
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET(SEARCH_PATH)
    suspend fun getSearchByQuery(
        @Query("q") query: String, @Query("key") apiKey: String
    ): List<LocationResponse>

    @GET(FORECAST_PATH)
    suspend fun getForecastData(
        @Query("q") query: String, @Query("key") apiKey: String, @Query("days") days: String
    ): ForecastResponse

}