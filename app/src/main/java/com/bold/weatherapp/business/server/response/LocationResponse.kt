package com.bold.weatherapp.business.server.response

data class LocationResponse(
    var name: String? = null,
    var region: String? = null,
    var country: String? = null
)
