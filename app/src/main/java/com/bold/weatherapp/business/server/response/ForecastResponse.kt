package com.bold.weatherapp.business.server.response

import com.google.gson.annotations.SerializedName

data class ForecastResponse(
    var location: LocationResponse = LocationResponse(),
    var current: CurrentResponse = CurrentResponse(),
    var forecast: Forecast = Forecast()
)

data class ConditionResponse(
    var text: String? = null, var icon: String? = null
)

data class CurrentResponse(
    @SerializedName("temp_c") var tempC: Double? = null,
    var condition: ConditionResponse? = ConditionResponse(),
    @SerializedName("wind_kph") var windKph: Double? = null,
    var humidity: Int? = null,
    @SerializedName("vis_km") var visKm: Int? = null
)

data class DayResponse(
    var condition: ConditionResponse? = ConditionResponse(),
    @SerializedName("maxtemp_c") var maxtempC: Double? = null,
    @SerializedName("mintemp_c") var mintempC: Double? = null,
)

data class ForecastDayResponse(
    var date: String? = null,
    var day: DayResponse? = DayResponse()
)

data class Forecast(
    var forecastday: List<ForecastDayResponse> = arrayListOf()
)