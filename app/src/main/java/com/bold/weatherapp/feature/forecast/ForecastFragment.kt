package com.bold.weatherapp.feature.forecast

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bold.weatherapp.R
import com.bold.weatherapp.common.launchAndCollect
import com.bold.weatherapp.databinding.FragmentForecastBinding
import com.bold.weatherapp.domain.LocationDomain
import com.bold.weatherapp.feature.forecast.adapter.ForecastAdapter
import com.bold.weatherapp.feature.forecast.adapter.SearchAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

@AndroidEntryPoint
class ForecastFragment : Fragment(R.layout.fragment_forecast),
    SearchAdapter.OnLocationClickListener {

    private lateinit var binding: FragmentForecastBinding
    private val forecastViewModel: ForecastViewModel by viewModels()
    private lateinit var forecastAdapter: ForecastAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentForecastBinding.bind(view)

        binding.rvForecastday.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ForecastFragment.requireContext())
        }

        binding.rvForecastday.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )

        binding.rvLocations.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ForecastFragment.requireContext())
        }

        binding.svCountries.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText.let {
                    forecastViewModel.getRemoteSearch(it!!)
                }

                return false
            }
        })

        initObservables()
    }

    private fun initObservables() {
        with(forecastViewModel.state) {
            diff({ it.loading }) { handleVisibility(it) }
            diff({ it.locations }) {
                if (it != null) {
                    binding.rvLocations.visibility = View.VISIBLE
                    binding.rvLocations.adapter = SearchAdapter(it, this@ForecastFragment)
                }
            }
            diff({ it.forecastDomain }) {
                if (it != null) {
                    binding.rvLocations.visibility = View.GONE

                    forecastAdapter = ForecastAdapter(it.forecast.forecastday, requireContext())
                    binding.rvForecastday.adapter = forecastAdapter


                    binding.tvWind.text = "Wind\n${it.current.windKph} km/h"
                    binding.tvHum.text = "Humidity\n${it.current.humidity}%"
                    binding.tvVis.text = "Visibility\n${it.current.visKm} km"
                    binding.tvTemp.text = "${it.current.tempC.toString()}°C"
                    binding.tvLocation.text = "${it.location.name}, ${it.location.region}"
                }
            }
        }
    }

    private fun handleVisibility(loading: Boolean) {
        binding.progressBar.isVisible = loading
    }

    override fun onLocationClick(locationDomain: LocationDomain) {
        locationDomain.let {
            forecastViewModel.getRemoteForecastUseCase(it.name!!)
        }
    }

    private fun <T, U> Flow<T>.diff(mapFlow: (T) -> U, body: (U) -> Unit) {
        viewLifecycleOwner.launchAndCollect(
            flow = map(mapFlow).distinctUntilChanged(), body = body
        )
    }

}