package com.bold.weatherapp.feature.forecast.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bold.weatherapp.common.BaseViewHolder
import com.bold.weatherapp.databinding.ItemLocationBinding
import com.bold.weatherapp.domain.LocationDomain

class SearchAdapter(
    private var locationList: List<LocationDomain>,
    private val itemClickListener: OnLocationClickListener
) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    interface OnLocationClickListener {
        fun onLocationClick(locationDomain: LocationDomain)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val itemBinding = ItemLocationBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return LocationViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is LocationViewHolder -> holder.bind(locationList[position])
        }
    }

    override fun getItemCount(): Int = locationList.size

    private inner class LocationViewHolder(
        val binding: ItemLocationBinding
    ) : BaseViewHolder<LocationDomain>(binding.root) {
        @SuppressLint("SetTextI18n")
        override fun bind(item: LocationDomain) {

            itemView.setOnClickListener { itemClickListener.onLocationClick(item) }

            binding.tvName.text = item.name
            binding.tvRegion.text = item.region
            binding.tvCountry.text = item.country
        }
    }

}