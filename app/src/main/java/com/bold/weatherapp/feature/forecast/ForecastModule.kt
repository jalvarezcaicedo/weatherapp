package com.bold.weatherapp.feature.forecast

import com.bold.weatherapp.data.repository.ForecastRepository
import com.bold.weatherapp.data.repository.SearchRepository
import com.bold.weatherapp.usecase.GetLocalForecastUseCase
import com.bold.weatherapp.usecase.GetRemoteForecastUseCase
import com.bold.weatherapp.usecase.GetRemoteSearchUseCase
import com.bold.weatherapp.usecase.RemoveForecastUseCase
import com.bold.weatherapp.usecase.SaveForecastUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class ForecastModule {

    @Provides
    @ViewModelScoped
    fun getLocalForecastUseCase(
        forecastRepository: ForecastRepository
    ) = GetLocalForecastUseCase(forecastRepository)

    @Provides
    @ViewModelScoped
    fun getRemoteForecastUseCase(
        forecastRepository: ForecastRepository
    ) = GetRemoteForecastUseCase(forecastRepository)

    @Provides
    @ViewModelScoped
    fun getRemoteSearchUseCase(
        searchRepository: SearchRepository
    ) = GetRemoteSearchUseCase(searchRepository)

    @Provides
    @ViewModelScoped
    fun saveForecastUseCase(
        forecastRepository: ForecastRepository
    ) = SaveForecastUseCase(forecastRepository)

    @Provides
    @ViewModelScoped
    fun removeForecastUseCase(
        forecastRepository: ForecastRepository
    ) = RemoveForecastUseCase(forecastRepository)

}