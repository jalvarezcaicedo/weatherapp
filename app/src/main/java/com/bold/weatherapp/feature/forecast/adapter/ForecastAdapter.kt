package com.bold.weatherapp.feature.forecast.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bold.weatherapp.common.BaseViewHolder
import com.bold.weatherapp.databinding.ItemForecastdayBinding
import com.bold.weatherapp.domain.Forecastday
import com.bumptech.glide.Glide
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class ForecastAdapter(
    private var forecastList: List<Forecastday>,
    private var context: Context
) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val itemBinding = ItemForecastdayBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ForecastViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is ForecastViewHolder -> holder.bind(forecastList[position])
        }
    }

    override fun getItemCount(): Int = forecastList.size

    private inner class ForecastViewHolder(
        val binding: ItemForecastdayBinding
    ) : BaseViewHolder<Forecastday>(binding.root) {
        @SuppressLint("SetTextI18n")
        override fun bind(item: Forecastday) {
            val formatter = DateTimeFormatter.ofPattern("EEE, MMM d")

            binding.tvDate.text = LocalDate.parse(item.date).format(formatter)

            Glide.with(context).load("https:" + item.day?.condition?.icon)
                .placeholder(android.R.drawable.ic_menu_gallery).centerCrop()
                .into(binding.ivIcon)

            binding.tvTemp.text = "${item.day?.mintempC}°C ${item.day?.maxtempC}°"
        }
    }

}