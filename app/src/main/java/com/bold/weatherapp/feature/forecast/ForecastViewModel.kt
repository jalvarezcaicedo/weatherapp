package com.bold.weatherapp.feature.forecast

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bold.weatherapp.domain.Error
import com.bold.weatherapp.domain.ForecastDomain
import com.bold.weatherapp.domain.LocationDomain
import com.bold.weatherapp.usecase.GetLocalForecastUseCase
import com.bold.weatherapp.usecase.GetRemoteForecastUseCase
import com.bold.weatherapp.usecase.GetRemoteSearchUseCase
import com.bold.weatherapp.usecase.RemoveForecastUseCase
import com.bold.weatherapp.usecase.SaveForecastUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ForecastViewModel @Inject constructor(
    private val getLocalForecastUseCase: GetLocalForecastUseCase,
    private val getRemoteForecastUseCase: GetRemoteForecastUseCase,
    private val getRemoteSearchUseCase: GetRemoteSearchUseCase,
    private val removeForecastUseCase: RemoveForecastUseCase,
    private val saveForecastUseCase: SaveForecastUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(UIState())
    val state: StateFlow<UIState> = _state.asStateFlow()

    fun getLocalForecast() = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getLocalForecastUseCase().catch {
            _state.update { UIState(error = it.error, loading = false) }
        }.collect {
            _state.update { UIState(forecastDomain = it.forecastDomain, loading = false) }
        }
    }

    fun getRemoteSearch(query: String) = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getRemoteSearchUseCase(query).fold(
            ifLeft = { error ->
                _state.update { it.copy(error = error, loading = false) }
            }, ifRight = { response ->
                _state.update { it.copy(locations = response, loading = false) }
            })
    }

    fun getRemoteForecastUseCase(query: String) = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getRemoteForecastUseCase.invoke(query).fold(
            ifLeft = { error ->
                _state.update { it.copy(error = error, loading = false) }
            }, ifRight = { response ->
                //removeForecast()
                //saveForecast()
                _state.update { it.copy(forecastDomain = response, loading = false) }
            })
    }

    private fun saveForecast(forecastDomain: ForecastDomain) = viewModelScope.launch {
        saveForecastUseCase(forecastDomain)
    }

    private fun removeProductsPreview() = viewModelScope.launch(Dispatchers.IO) {
        removeForecastUseCase.invoke()
    }

    data class UIState(
        val error: Error? = null,
        val loading: Boolean = false,
        val locations: List<LocationDomain>? = null,
        val forecastDomain: ForecastDomain? = null
    )

}