package com.bold.weatherapp.module

import android.app.Application
import com.bold.weatherapp.business.database.WeatherDB
import com.bold.weatherapp.business.database.source.LocalForecastDataSourceImpl
import com.bold.weatherapp.business.server.service.WeatherApi
import com.bold.weatherapp.business.server.source.RemoteForecastDataSourceImpl
import com.bold.weatherapp.business.server.source.RemoteSearchDataSourceImpl
import com.bold.weatherapp.data.repository.ForecastRepository
import com.bold.weatherapp.data.repository.SearchRepository
import com.bold.weatherapp.data.source.LocalForecastDataSource
import com.bold.weatherapp.data.source.RemoteForecastDataSource
import com.bold.weatherapp.data.source.RemoteSearchDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class Data {

    @Provides
    @Singleton
    fun databaseProvider(app: Application): WeatherDB = WeatherDB.getDatabase(app)

    @Provides
    fun searchRepository(
        remoteSearchDataSource: RemoteSearchDataSource
    ) = SearchRepository(remoteSearchDataSource)

    @Provides
    fun forecastRepository(
        localForecastDataSource: LocalForecastDataSource,
        remoteForecastDataSource: RemoteForecastDataSource
    ) = ForecastRepository(localForecastDataSource, remoteForecastDataSource)

    @Provides
    fun localForecastDataSource(
        db: WeatherDB
    ): LocalForecastDataSource = LocalForecastDataSourceImpl(db)

    @Provides
    fun remoteSearchDataSource(
        weatherApi: WeatherApi
    ): RemoteSearchDataSource = RemoteSearchDataSourceImpl(weatherApi)

    @Provides
    fun remoteForecastDataSource(
        weatherApi: WeatherApi
    ): RemoteForecastDataSource = RemoteForecastDataSourceImpl(weatherApi)

}